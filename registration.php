<?php
/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

use Magento\Framework\Component\ComponentRegistrar;


ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Optiweb_DoubleCurrency', __DIR__);
