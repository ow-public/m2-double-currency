/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Catalog/js/price-utils',
], function (Component, quote, priceUtils) {
    'use strict';

    return Component.extend({

        initialize: function () {
            this.totals = quote.getTotals();
            return this._super();
        },

        /**
         * @returns {boolean}
         */
        enabled: function () {
            return this.dc_config.enable === '1';
        },

        /**
         * @returns {String}
         */
        secondaryCurrency: function () {
            var subtotal = parseFloat(this.totals()['base_grand_total']);
            return priceUtils.formatPrice(subtotal * this.dc_config.conversion_rate, {
                requiredPrecision: 2,
                integerRequired: 1,
                decimalSymbol: ',',
                groupSymbol: '.',
                groupLength: 3,
                pattern: '%s ' + this.dc_config.secondary_currency
            })
        },

        /**
         * @returns {boolean}
         */
        displayConversionRate: function () {
            return this.dc_config.display_conversion_rate_cart === '1';
        },

        /**
         * @returns {String}
         */
        conversionRateText: function () {
            return this.dc_config.conversion_rate_cart_text;
        },

    })
})