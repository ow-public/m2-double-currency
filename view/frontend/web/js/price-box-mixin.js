/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

define([
    'jquery',
    'Magento_Catalog/js/price-utils',
    'underscore',
    'mage/template',
], function ($, utils, _, mageTemplate) {
    return function (priceBoxWidget) {
        $.widget('mage.priceBox', priceBoxWidget, {

            reloadPrice: function reDrawPrices() {
                this._super();

                if (typeof window.doubleCurrencyConfig !== 'object') {
                    return;
                }

                var priceFormat = window.doubleCurrencyConfig.format || {},
                    secondaryPriceTemplate = mageTemplate('<span class="price"><%- text %></span>'),
                    rate  = window.doubleCurrencyConfig.rate || 1;

                _.each(this.cache.displayPrices, function (price, priceCode) {
                    price.final = _.reduce(price.adjustments, function (memo, amount) {
                        return memo + amount;
                    }, price.amount);

                    $(this.element).find('.informative-price[data-price-type='+ priceCode +']').html(secondaryPriceTemplate({
                        text: utils.formatPrice(price.amount * rate, priceFormat, false)
                    }));
                }, this);

            },

        });
        return $.mage.priceBox;
    }
})