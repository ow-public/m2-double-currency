<?php
/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\DoubleCurrency\Block\Cart;

use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\NumberFormatter;
use Magento\Store\Model\ScopeInterface;
use NumberFormatter as NumberFormatterAlias;
use Optiweb\DoubleCurrency\Helper\Data;


class LayoutProcessor implements LayoutProcessorInterface
{
    /** @var ScopeConfigInterface  */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param $jsLayout
     * @return array
     */
    public function process($jsLayout): array
    {
        $doubleCurrencyConfig = $this->_scopeConfig->getValue(Data::CONFIG_PATH, ScopeInterface::SCOPE_STORE);

        if ($doubleCurrencyConfig['display_conversion_rate_cart'] === '1') {
            $locale = $this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE);
            $currencyFormatter = new NumberFormatter($locale, NumberFormatterAlias::CURRENCY);
            $numberFormatter = new NumberFormatter($locale, NumberFormatterAlias::DECIMAL);
            $baseCurrency = $this->_scopeConfig->getValue('currency/options/base', ScopeInterface::SCOPE_STORE);
            $doubleCurrencyConfig['conversion_rate_cart_text'] = __('Conversion rate: %1. 1 %2 = %3',
                $numberFormatter->format($doubleCurrencyConfig['conversion_rate']),
                $baseCurrency,
                $currencyFormatter->formatCurrency($doubleCurrencyConfig['conversion_rate'], $doubleCurrencyConfig['secondary_currency']),
            );
        }
        $jsLayout['components']['block-totals']['children']['double_currency']['config']['dc_config'] = $doubleCurrencyConfig;
        return $jsLayout;
    }
}
