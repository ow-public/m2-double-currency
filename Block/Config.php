<?php
/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */
declare(strict_types=1);

namespace Optiweb\DoubleCurrency\Block;

use Magento\Framework\Locale\Format;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;
use Optiweb\DoubleCurrency\Helper\Data;


class Config extends Template
{
    /** @var Format */
    protected Format $_format;

    /** @var Json */
    protected Json $_json;

    /**
     * @param Template\Context $context
     * @param Format $format
     * @param Json $json
     */
    public function __construct(
        Template\Context $context,
        Format $format,
        Json $json
    ) {
        $this->_format = $format;
        $this->_json = $json;
        parent::__construct($context);
    }

    /**
     * @return string
     */
    public function getJsonConfig(): string
    {
        $locale = $this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE);
        $secondaryCurrency = $this->_scopeConfig->getValue(Data::CONFIG_PATH . '/secondary_currency', ScopeInterface::SCOPE_STORE);

        return $this->_json->serialize([
            'format' => $this->_format->getPriceFormat($locale, $secondaryCurrency),
            'rate' => (float)$this->_scopeConfig->getValue(Data::CONFIG_PATH . '/conversion_rate', ScopeInterface::SCOPE_STORE) ?? 0,
        ]);
    }
}
