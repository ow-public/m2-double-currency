<?php
/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\DoubleCurrency\Block\Product\View;

use Magento\Framework\NumberFormatter;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;
use NumberFormatter as NumberFormatterAlias;
use Optiweb\DoubleCurrency\Helper\Data;


class ConversionRate extends Template
{
    /**
     * @return string
     */
    public function getConversionText(): string
    {
        $configuration = $this->_scopeConfig->getValue(Data::CONFIG_PATH, ScopeInterface::SCOPE_STORE);
        $baseCurrency = $this->_scopeConfig->getValue('currency/options/base', ScopeInterface::SCOPE_STORE);
        if (!$configuration['enable'] || !$configuration['display_conversion_rate_productpage']) {
            return '';
        }
        $locale = $this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE);
        $currencyFormatter = new NumberFormatter($locale, NumberFormatterAlias::CURRENCY);
        $numberFormatter = new NumberFormatter($locale, NumberFormatterAlias::DECIMAL);

        return __('Conversion rate: %1. 1 %2 = %3',
            $numberFormatter->format($configuration['conversion_rate']),
            $baseCurrency,
            $currencyFormatter->formatCurrency($configuration['conversion_rate'], $configuration['secondary_currency']),
        );
    }
}