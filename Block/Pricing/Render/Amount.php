<?php
/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\DoubleCurrency\Block\Pricing\Render;

use Magento\Framework\Pricing\Render\Amount as Subject;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\NumberFormatter;
use Optiweb\DoubleCurrency\Helper\Data;


class Amount extends Subject
{
    /**
     * @return bool
     */
    public function isDoublePriceEnabled(): bool
    {
        return (bool)$this->_getDoublePriceConfig()['enable'];
    }

    /**
     * @return string
     */
    public function getSecondaryCurrencySymbol(): string
    {
        return $this->_getDoublePriceConfig()['secondary_currency'] ?? '';
    }

    /**
     * @return float
     */
    public function getConversionRate(): float
    {
        return (float)$this->_getDoublePriceConfig()['conversion_rate'] ?? 1;
    }

    /**
     * @param float $originalAmount
     * @return string
     */
    public function getSecondaryAmount(float $originalAmount): string
    {
        $formatter = new NumberFormatter($this->_getLocale());

        return $formatter->formatCurrency(
            $originalAmount * $this->getConversionRate(),
            $this->getSecondaryCurrencySymbol()
        );
    }

    /**
     * @return array
     */
    private function _getDoublePriceConfig(): array
    {
        return $this->_scopeConfig->getValue(Data::CONFIG_PATH, ScopeInterface::SCOPE_STORE) ?? [];
    }

    /**
     * @return string
     */
    private function _getLocale(): string
    {
        return $this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE);
    }
}