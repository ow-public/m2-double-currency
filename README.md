# Optiweb_DoubleCurrency
## Magento 2 module

This module is used to display prices in a secondary currency in various places including and not limited to:
 - product page - main info
 - search autocomplete
 - minicart
 - cart items
 - cart sidebar subtotal
 - upsell / related products
 - ...

![Product page](media/product_view.png)
![Category page / search](media/category_view.png)
![Cart summary](media/cart_summary.png)
![Checkout summary](media/checkout_summary.png)

### Features
 - Enable/disable functionality per store view
 - Use any [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes) secondary currency. It is rendered according to your default locale using PHP [NumberFormatter::formatCurrency](https://www.php.net/manual/en/numberformatter.formatcurrency.php)
 - Configure conversion rate in system configuration
 - Display said conversion rate on product page and/or cart and checkout summary

### Instructions
Navigate to Stores → Configuration → Catalog → Catalog → Double (informative) currency.
1. **Enable** - enable or disable this feature
2. **Secondary currency** - any [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes) currency code
3. **Conversion rate** - 1 unit of your primary currency amounts to this amount of secondary currency. **Warning**: this value is fixed and does not auto update! Defaults to 1 - no conversion.
4. **Display conversion rate on product page** - Display conversion rate information on product page, for example: "Conversion rate: 0.13272. 1 HRK = 0.13 EUR"
5. **Display conversion rate on cart summary** - Display conversion rate information on cart and checkout summary"

### Compatibility
 - Developed on a Magento 2.4.3 site with PHP 7.4
 - Compatible with Magento >= 2.3.7 running on PHP 7.4 or higher

### Changelog
 - `1.0.0`
   - Initial release
 - `1.1.0`
   - Update secondary currency on configurable product page in when product options have different prices
 - `1.1.1`
   - Fix JS error if feature is disabled
 - `1.1.2`
   - Fix issue with different price types

### License
MIT - See [LICENSE.txt](LICENSE.txt)