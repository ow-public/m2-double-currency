<?php
/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\DoubleCurrency\Plugin;

use Magento\Sales\Block\Order\Email\Items\Order\DefaultOrder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Sales\Model\Order\Item as OrderItem;
use Magento\Framework\NumberFormatter;
use Optiweb\DoubleCurrency\Helper\Data;


class OrderEmailItem
{
    /** @var ScopeConfigInterface */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param DefaultOrder $subject
     * @param string $result
     * @param OrderItem $item
     * @return string
     */
    public function afterGetItemPrice(DefaultOrder $subject, string $result, OrderItem $item): string
    {
        $configuration = $this->_scopeConfig->getValue(Data::CONFIG_PATH, ScopeInterface::SCOPE_STORE);
        if (!$configuration['enable']) {
            return $result;
        }
        $total = $item->getPriceInclTax();
        $formatter = new NumberFormatter($this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE));
        $result .= "<div class='informative-price'>{$formatter->formatCurrency($total * $configuration['conversion_rate'], $configuration['secondary_currency'])}</div>";

        return $result;
    }
}