<?php
/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\DoubleCurrency\Plugin\Checkout;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Checkout\Block\Checkout\LayoutProcessor as Subject;
use Optiweb\DoubleCurrency\Helper\Data;
use NumberFormatter as NumberFormatterAlias;
use Magento\Framework\NumberFormatter;


class LayoutProcessor
{
    /** @var ScopeConfigInterface  */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param Subject $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(Subject $subject, array $jsLayout)
    {
        $doubleCurrencyConfig = $this->_scopeConfig->getValue(Data::CONFIG_PATH, ScopeInterface::SCOPE_STORE);

        if ($doubleCurrencyConfig['display_conversion_rate_cart'] === '1') {
            $locale = $this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE);
            $currencyFormatter = new NumberFormatter($locale, NumberFormatterAlias::CURRENCY);
            $numberFormatter = new NumberFormatter($locale, NumberFormatterAlias::DECIMAL);
            $baseCurrency = $this->_scopeConfig->getValue('currency/options/base', ScopeInterface::SCOPE_STORE);
            $doubleCurrencyConfig['conversion_rate_cart_text'] = __('Conversion rate: %1. 1 %2 = %3',
                $numberFormatter->format($doubleCurrencyConfig['conversion_rate']),
                $baseCurrency,
                $currencyFormatter->formatCurrency($doubleCurrencyConfig['conversion_rate'], $doubleCurrencyConfig['secondary_currency']),
            );
        }

        $jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children']['totals']
        ['children']['double_currency']['config']['dc_config'] = $doubleCurrencyConfig;
        return $jsLayout;
    }
}
