<?php
/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\DoubleCurrency\Plugin;

use Magento\Sales\Block\Order\Totals;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\NumberFormatter;
use Optiweb\DoubleCurrency\Helper\Data;


class OrderTotals
{
    /** @var ScopeConfigInterface */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param Totals $subject
     * @param string $result
     * @param DataObject $total
     * @return string
     */
    public function afterFormatValue(Totals $subject, string $result, $total): string
    {
        $configuration = $this->_scopeConfig->getValue(Data::CONFIG_PATH, ScopeInterface::SCOPE_STORE);
        if (!$configuration['enable']) {
            return $result;
        }
        $formatter = new NumberFormatter($this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE));

        $result .= "<div class='informative-price'>{$formatter->formatCurrency($total->getValue() * $configuration['conversion_rate'], $configuration['secondary_currency'])}</div>";

        return $result;
    }
}