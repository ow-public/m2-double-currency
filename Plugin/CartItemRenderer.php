<?php
/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\DoubleCurrency\Plugin;

use Magento\Checkout\Block\Cart\Item\Renderer;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\NumberFormatter;
use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\Store\Model\ScopeInterface;
use Optiweb\DoubleCurrency\Helper\Data;


class CartItemRenderer
{
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param Renderer $subject
     * @param string $result
     * @param AbstractItem $item
     * @return string
     */
    public function afterGetUnitPriceHtml(Renderer $subject, string $result, AbstractItem $item): string
    {
        $configuration = $this->_scopeConfig->getValue(Data::CONFIG_PATH, ScopeInterface::SCOPE_STORE);
        if (!$configuration['enable']) {
            return $result;
        }
        $formatter = new NumberFormatter($this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE));
        $total = $item->getPriceInclTax();
        $result .= "<div class='informative-price'>{$formatter->formatCurrency($total * $configuration['conversion_rate'], $configuration['secondary_currency'])}</div>";

        return $result;
    }
}