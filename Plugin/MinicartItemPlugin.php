<?php
/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\DoubleCurrency\Plugin;

use Magento\Checkout\CustomerData\Cart;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\NumberFormatter;
use Optiweb\DoubleCurrency\Helper\Data;


class MinicartItemPlugin
{
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param Cart $subject
     * @param array $result
     * @return array
     */
    public function afterGetSectionData(Cart $subject, array $result): array
    {
        $doubleCurrencyConfig = $this->_scopeConfig->getValue(Data::CONFIG_PATH, ScopeInterface::SCOPE_STORE);
        if (!$doubleCurrencyConfig['enable']) {
            return $result;
        }
        $formatter = new NumberFormatter($this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE), NumberFormatter::CURRENCY);
        $result['second_currency_total'] = $formatter->formatCurrency(
            $result['subtotalAmount'] * $doubleCurrencyConfig['conversion_rate'],
            $doubleCurrencyConfig['secondary_currency']
        );

        return $result;
    }
}