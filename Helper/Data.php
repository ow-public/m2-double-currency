<?php
/*
 * Author: Aleš Cankar (ales.cankar@optiweb.com)
 * Copyright (c) 2022 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\DoubleCurrency\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\NumberFormatter;


class Data extends AbstractHelper
{
    const CONFIG_PATH = 'catalog/double_currency';

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return (bool)$this->_getConfiguration()['enable'];
    }

    /**
     * @return string
     */
    public function getSecondaryCurrencySymbol(): string
    {
        return $this->_getConfiguration()['secondary_currency'] ?? '';
    }

    /**
     * @return float
     */
    public function getConversionRate(): float
    {
        return (float)$this->_getConfiguration()['conversion_rate'] ?? 1;
    }

    /**
     * @param float $originalAmount
     * @return string
     */
    public function getSecondaryAmount(float $originalAmount): string
    {
        $formatter = new NumberFormatter($this->_getLocale());

        return $formatter->formatCurrency(
            $originalAmount * $this->getConversionRate(),
            $this->getSecondaryCurrencySymbol()
        );
    }

    /**
     * @return array
     */
    private function _getConfiguration(): array
    {
        return $this->scopeConfig->getValue(Data::CONFIG_PATH, ScopeInterface::SCOPE_STORE) ?? [];
    }

    /**
     * @return string
     */
    private function _getLocale(): string
    {
        return $this->scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE);
    }

}